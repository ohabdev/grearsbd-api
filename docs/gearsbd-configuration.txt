=== Global ===
# required Ubuntu 16.04
# connect to server
	ssh -i /home/mobarokhossain/.ssh/simec-aws-key.pem ubuntu@ec2-52-15-186-108.us-east-2.compute.amazonaws.com
# install node v8.x
# enable <27017> port in server
# install redis

-----------
=== API ===
-----------
# Nodejs setup
	- run command if package related error happened: sudo apt install build-essential and sudo apt install tcl

# mongodb user create and conect
	- enable <27017> port in server
	- resource
	- db.createUser({
	      user: "gearsbd",
	      pwd: "password",
	      roles: [
                { role: "userAdmin", db: "genstore-dev" },
                { role: "dbAdmin",   db: "genstore-dev" },
                { role: "readWrite", db: "genstore-dev" }
             ]
	  });
	  - db basic commands: https://dzone.com/articles/top-10-most-common-commands-for-beginners
	  - https://medium.com/founding-ithaka/setting-up-and-connecting-to-a-remote-mongodb-database-5df754a4da89

# redis install
	- https://www.digitalocean.com/community/tutorials/how-to-install-and-configure-redis-on-ubuntu-16-04

-----------------
=== Frontend ===
-----------------
# change apiBaseUrl in /src/environments/environment.ts
- http://52.15.186.108:9000/v1


--------------
=== Seller ===
--------------
# change apiBaseUrl in /src/environments/environment.ts
- http://52.15.186.108:9000/v1

-------------
=== Admin ===
-------------
npm install node-sass
npm uninstall @angular-devkit/build-angular
npm install --save-dev @angular-devkit/build-angular

admin@gearsbd.com
test@shop.com
test@gearsbd.com